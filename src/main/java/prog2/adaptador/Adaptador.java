package prog2.adaptador;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import prog2.model.Article;
import prog2.model.Client;
import prog2.model.Comanda;
import prog2.model.Dades;
import prog2.vista.MercatException;

public class Adaptador implements Serializable{

    private Dades _dades;

    public Adaptador() {
        _dades = new Dades();
    }

    /**
     * Afegeix l'article desde la clase "Dades.java".
     * @param _id
     * @param _name
     * @param _price
     * @param _allow_urgent
     * @param _send_time
     */
    public void afegirArticle( String _id, String _name, float _price, boolean _allow_urgent, int _send_time) throws MercatException {
        _dades.afegirArticle(_id, _name, _price, _send_time, _allow_urgent);
    }

    /**
     * Retorna els articles de la clase "Dades.java".
     */

    public ArrayList<Article> recuperaArticles() {
        return _dades.recuperaArticles();
    }
    /**
     * Transforma els articles a un string de la clase "Dades.java".
     */
    
    public String articlesToString() {
        
        String _output = "";
        int _count = 0;

        Iterator<Article> it = _dades.recuperaArticles().iterator();
        while (it.hasNext()) {
            _output += String.format("[%d] %s \n", _count, it.next().toString());
            _count += 1;
        }

        return _output;
    }

    /**
     * Transforma la llista de articles a un Array de Arrays amb els atributs dels articles.
     */
    public ArrayList<ArrayList<Object>> articlesToStringWithFields() {
        
        ArrayList<ArrayList<Object>> _output = new ArrayList<>();

        Iterator<Article> it = _dades.recuperaArticles().iterator();
        while (it.hasNext()) {
            Article article = it.next();
            ArrayList<Object> temp = new ArrayList<>();
            temp.add(article.getId());
            temp.add(article.getName());
            temp.add(article.getPrice());
            temp.add(article.getSend_time().getSeconds());
            temp.add(article.isAllow_urgent());
            _output.add(temp);
        }

        return _output;
    }

    /**
     * Afegeix els clients desde la clase "Dades.java".
     */

    public void afegirClient(String email, String nom, String adreca, boolean esPremium) throws MercatException {
        _dades.afegirClient(email, nom, adreca, esPremium);
    }

    /**
     * Retorna els clientes desde la classe "Dades.java".
     */

    public ArrayList<Client> recuperaClients() {
        return _dades.recuperaClients();
    }

    /**
     * Transforma els clients a un string de la clase "Dades.java".
     */
    public String clientsToString() {
        
        String _output = "";
        int _count = 0;

        Iterator<Client> it = _dades.recuperaClients().iterator();
        while (it.hasNext()) {
            _output += String.format("[%d] %s \n", _count, it.next().toString());
            _count += 1;
        }

        return _output;
    }

    /**
     * Transforma la llista de clients a un Array de Arrays amb els atributs dels articles.
     */
    public ArrayList<ArrayList<Object>> clientsToStringWithFields() {
        
        ArrayList<ArrayList<Object>> _output = new ArrayList<>();
        int _count = 0;

        Iterator<Client> it = _dades.recuperaClients().iterator();
        while (it.hasNext()) {
            Client client = it.next();
            ArrayList<Object> temp = new ArrayList<>();
            temp.add(_count);
            temp.add(client.getName());
            temp.add(client.getEmail());
            temp.add(client.getAddress());
            temp.add(client.tipusClient().equals("Estandard") );
            temp.add(client.descompteEnv());
            temp.add(client.calcMensual());
            _output.add(temp);
            _count += 1;
        }

        return _output;
    }

    /**
     * Afegeix la comanda desde la clase "Dades.java".
     * @param articlePos
     * @param clientPos
     * @param quantitat
     * @param esUrgent
     * @throws MercatException
     */

    public void afegirComanda(int articlePos, int clientPos, int quantitat, boolean esUrgent) throws MercatException {
        _dades.afegirComanda(articlePos, clientPos, quantitat, esUrgent);
    }

    /**
     * Recupera les comandes de la clase "Dades.java".
     */

    public ArrayList<Comanda> recuperaComandes() {
        return _dades.recuperaComandes();
    }

    /**
     * Recupera les comandes cancel.lades de la clase "Dades.java".
     */

    public ArrayList<Comanda> recuperaComandesCancelades() {
        return _dades.recuperaComandesCancelades();
    }
    
     public ArrayList<ArrayList<Object>> comandesToStringWithFields() {
        
        ArrayList<ArrayList<Object>> _output = new ArrayList<>();

        Iterator<Comanda> it = _dades.recuperaComandes().iterator();
        while (it.hasNext()) {
            Comanda comanda = it.next();
            ArrayList<Object> temp = new ArrayList<>();
            temp.add(comanda.getProduct().getName());
            temp.add(comanda.getCustomer().getName());
            temp.add(comanda.getQuantity());
            temp.add(comanda.tipusComanda().equals("Normal"));
            temp.add(comanda.calcPreu());
            temp.add(comanda.preuEnviament());
            temp.add(comanda.isSent());
            temp.add(comanda.isReceived());
            temp.add(comanda.isCanceled());
            _output.add(temp);
        }

        return _output;
    }
     
     public ArrayList<ArrayList<Object>> comandesCanceladesToStringWithFields() {
        
        ArrayList<ArrayList<Object>> _output = new ArrayList<>();

        Iterator<Comanda> it = _dades.recuperaComandesCancelades().iterator();
        while (it.hasNext()) {
            Comanda comanda = it.next();
            ArrayList<Object> temp = new ArrayList<>();
            temp.add(comanda.getProduct().getName());
            temp.add(comanda.getCustomer().getName());
            temp.add(comanda.getQuantity());
            temp.add(comanda.tipusComanda().equals("Normal"));
            temp.add(comanda.calcPreu());
            temp.add(comanda.preuEnviament());
            temp.add(comanda.isSent());
            temp.add(comanda.isReceived());
            temp.add(comanda.isCanceled());
            _output.add(temp);
        }

        return _output;
    }
    

    /**
     * Retorna les comandes en un String
     * @param nomesCancelades
     */
    public String comandesToString(boolean nomesCancelades) {
        
        StringBuilder _output = new StringBuilder("");
        AtomicInteger _count = new AtomicInteger(0);

        _dades.recuperaComandes().forEach( (comanda) -> {

            if (!nomesCancelades || (nomesCancelades && comanda.isCanceled())) {
                _output.append(String.format("[%d] %s \n", _count.getAndIncrement(), comanda.toString()));
            }
        });
        
        return _output.toString();
    }

    /**
     * Cancel.la la comanda segons posició del parámetre
     * @param pos
     * @throws MercatException
     */

    public void cancelarComanda(int pos) throws MercatException {
        _dades.cancelarComanda(pos);
    }

    /**
     * Serialitza les dades dels articles i comandes.
     * @param file
     * @throws Exception
     */
    public void saveToFile(File file) throws Exception {

        FileOutputStream fout = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(this._dades);

        oos.close();
        fout.close();
    }

    /**
     * Carrega les dades
     * @param file
     * @throws Exception
     */

    public void loadFromFile(File file) throws Exception {
        FileInputStream fin = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fin);
        this._dades = (Dades) ois.readObject();
        
        ois.close();
        fin.close();
    }

}