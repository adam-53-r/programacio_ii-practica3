package prog2.vista;

public class MercatException extends Exception {

    public MercatException(){}

    /**
     * @param string
     */
    public MercatException(String string) {
        super(string);
    }
    
}
