package prog2.vista;

import prog2.adaptador.Adaptador;

import java.util.Scanner;
import java.awt.GraphicsEnvironment;
import java.io.File;
import javax.swing.JFileChooser;

public class MercatUB {

    private Adaptador _adaptador;

    enum llistaOpcions {
        MENU_GESTIO_ARTICLES,
        MENU_GESTIO_CLIENTS,
        MENU_GESTIO_COMANDES,
        MENU_GUARDAR_DADES,
        MENU_CARREGA_DADES,
        MENU_SORTIR
    };

    String[] descripcions = {
        "Gestió Articles",
        "Gestió Clients",
        "Gestió Comandes",
        "Guardar Dades",
        "Carrega Dades",
        "Sortir"
    };

    public MercatUB() {
        _adaptador = new Adaptador();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////// HELPER METHODS ///////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Imprimeix el missatge en verd.
     *
     * @param message
     */
    public void printSuccess(String message) {
        System.out.print(String.format("\033[32m%s\033[0m", message));
    }

    /**
     * Imprimeix el missatge en vermell.
     *
     * @param message
     */
    public void printError(String message) {
        System.out.print(String.format("\033[31m%s\033[0m", message));
    }

    /**
     * Li demana una entrada al usuari.
     *
     * @param sc
     * @param message
     * @return
     */
    public String input(Scanner sc, String message) {
        System.out.print(message);
        System.out.print("\033[32m>> \033[0m");
        return sc.nextLine();
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un int, sino ho
     * ses li demana una altra vegada
     *
     * @param sc
     * @param message
     * @return
     */
    public int inputInt(Scanner sc, String message) {
        
        int response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextInt();
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un int dintre
     * del rang especificat amb min i max, sino ho ses li demana una altra
     * vegada
     *
     * @param sc
     * @param message
     * @param min
     * @param max
     * @return
     */
    public int inputInt(Scanner sc, String message, int min, int max) {
        
        int response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextInt();
                if (response < min || response > max) {
                    printError(String.format("Error: Introdueix un numero entre %d i %d.\n", min, max));
                    continue;
                }
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un float, sino
     * ho ses li demana una altra vegada
     *
     * @param sc
     * @param message
     * @return
     */
    public float inputFloat(Scanner sc, String message) {
        
        float response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextFloat();
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    /**
     * Li demana un int al usuari, comprova que la entrada sigui un float dintre
     * del rang especificat amb min i max, sino ho ses li demana una altra
     * vegada
     *
     * @param sc
     * @param message
     * @param min
     * @param max
     * @return
     */
    public float inputFloat(Scanner sc, String message, float min, float max) {
        
        float response;

        while (true) {
            System.out.print(message);
            System.out.print("\033[32m>> \033[0m");
            try {
                response = sc.nextFloat();
                if (response < min || response > max) {
                    printError(String.format("Error: Introdueix un numero entre %f i %f.\n", min, max));
                    continue;
                }
                break;
            } catch (Exception e) {
                printError("Error: Introdueix un numero valid.\n");
            }
            sc.nextLine();
        }
        sc.nextLine();
        return response;
    }

    public boolean inputBoolean(Scanner sc, String message) {
        System.out.print(message);
        System.out.print("\033[32m>> \033[0m");
        return sc.nextLine().equalsIgnoreCase("y");
    }

    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// MENU METHODS ////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Crea un menú on es pot afegir y visualitzar clients.
     * @param sc
     */
    void gestio_articles(Scanner sc) {

        int choice = inputInt(
            sc,
            "Sel·lecciona una opcio: (0-2)\n"
            + " [0] Afegir un article\n"
            + " [1] Visualitzar articles\n"
            + " [2] Sortir\n",
            0,
            2
        );

        switch (choice) {
            case 0:
                String _name = input(sc, "Introdueix el nom de l'article:\n");
                float _price = inputFloat(sc, "Introdueix el preu de l'article:\n");
                boolean _allow_urgent = inputBoolean(sc, "Introdueix si l'article permet enviament urgent o no: (Y/n)\n");
                int _send_time = inputInt(sc, "Introdueix el temps d'enviament de l'article:\n");
                
                try {
                    _adaptador.afegirArticle(_name, _name, _price, _allow_urgent, _send_time); 
                    printSuccess("Article afegit correctament.\n");                   
                } catch (Exception e) {
                    printError("Error: " + e.getMessage() + "\n");
                }
                break;
        
            case 1:
                System.out.println(_adaptador.articlesToString());
                break;
        
            default:
            System.out.println("Going back to main menu.");
                break;
        }

    }

    /**
     * Crea un menú on es podrà afegir i visualitzar clients.
     * @param sc
     */
    void gestio_clients(Scanner sc) {
        boolean isRunning = true;
        while (isRunning) {
            System.out.println("[1] Añadir clientes");
            System.out.println("[2] Visualizar clientes");
            System.out.println("[3] Salir");
            System.out.println("Selecciona una opción: ");
            int option = sc.nextInt();
            sc.nextLine();

            switch (option) {
                case 1:
                    int tipusClient = inputInt(sc,
                        "Selecciona tipo de cliente:\n" +
                        "[1] Premium\n" + "[2] Estándard\n",
                        1, 2);

                    boolean esPremium = false;

                    if (tipusClient == 1) {
                        System.out.println("Has seleccionat client premium");
                        esPremium = true;
                    }
                    if (tipusClient == 2) {
                        System.out.println("Has seleccionat client estandard");
                    }

                    String name = input(sc, "Nom del client: \n");
                    String email = input(sc, "Email del client " + name + " :\n");
                    String address = input(sc, "Adreça del client: \n");
                    
                    try {
                        _adaptador.afegirClient(email, name, address, esPremium);
                        printSuccess("S'ha creat el client " + (esPremium ? "premium" : "estandard") + " amb el nom: " + name + " amb l'email: " + email + " i l'adreça: " + address + "\n");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                    break;
                case 2:
                    System.out.println("Aquests son els clients: ");
                    System.out.println(_adaptador.clientsToString());
                    break;
                case 3:
                    isRunning = false;
                    System.out.println("Has sortit. ");
                    break;
                default:
                    System.out.println("Opció incorrecte!");
                    break;
            }
        }
    }

    /**
     * Crea un menú on es podrà afegir comandes, cancel.lar comandes, visualitzar comandes i les comandes cancel.lades
     * @param sc
     */

    void gestio_comandes(Scanner sc) {
        int choice = inputInt(
            sc,
            "Sel·lecciona una opcio: (1-5)\n"
            + " [1] Afegir una comanda\n"
            + " [2] Cancel·lar una comanda\n"
            + " [3] Visualitzar Comandes\n"
            + " [4] Visualitzar Comandes Cancel·lades\n"
            + " [5] Sortir\n",
            1,
            5
        );

        switch (choice) {
            case 1:
                System.out.println(_adaptador.articlesToString());
                int _articlePos = inputInt(sc, "Introdueix la posicio de l'article a usar:\n");
                System.out.println(_adaptador.clientsToString());
                int _clientPos = inputInt(sc, "Introdueix la posicio del client a usar:\n");
                int _quantitat = inputInt(sc, "Introdueix la quantitat d'articles:\n");
                boolean _esUrgent = inputBoolean(sc, "Introdueix si la comanda es urgent: (Y/n)\n");
                
                try {
                    _adaptador.afegirComanda(_articlePos, _clientPos, _quantitat, _esUrgent);
                    printSuccess("Comanda afegida correctament.\n");                   
                } catch (Exception e) {
                    printError("Error: " + e.getMessage() + "\n");
                }
                break;
        
            case 2:
                System.out.println( _adaptador.comandesToString(false) );
                int pos = inputInt(sc, "Introdueix el numero de comanda a cancel·lar:\n");
                try {
                    _adaptador.cancelarComanda(pos);
                    printSuccess("Comanda cancel·lada correctament.\n");
                } catch (Exception e) {
                    printError("Error: " + e.getMessage() + "\n");
                }
                break;
        
            case 3:
                System.out.println( _adaptador.comandesToString(false) );
                break;
        
            case 4:
                System.out.println( _adaptador.comandesToString(true) );
                break;
        
            default:
            System.out.println("Going back to main menu.");
                break;
        }
    }

    /**
     * Guarda les dades de una manera gràfica
     * @param sc
     */

    void guardar_dades(Scanner sc) {

        // Es possible que no funcioni per alguna rao desconeguda
        if (!GraphicsEnvironment.isHeadless() && false) {
        // if (!GraphicsEnvironment.isHeadless()) {

            // Create a new file chooser
            JFileChooser fileChooser = new JFileChooser();

            // Set the file chooser to "Save" mode
            fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);

            while (true) {
                // Set the current directory of the chooser
                fileChooser.setCurrentDirectory(new File("."));
                // Show the system "Save As" dialog
                fileChooser.showSaveDialog(null);
                try {
                    // Get the selected file
                    File file = fileChooser.getSelectedFile();
                    file.canRead();
                    file.canWrite();
                    _adaptador.saveToFile(file);
                    printSuccess("Info saved successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        } else {

            while (true) {
                try {
                    File file = new File(input(sc, "Save path (file):\n"));
                    file.canRead();
                    file.canWrite();
                    _adaptador.saveToFile(file);
                    printSuccess("Info saved successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Carrega les dades.
     * @param sc
     */

    void carrega_dades(Scanner sc) {
        // Es possible que no funcioni per alguna rao desconeguda
        if (!GraphicsEnvironment.isHeadless() && false) {
        // if (!GraphicsEnvironment.isHeadless()) {

            // Create a new file chooser
            JFileChooser fileChooser = new JFileChooser();
    
            // Set the file chooser to "Save" mode
            fileChooser.setDialogType(JFileChooser.FILES_ONLY);
    
            while (true) {
                // Set the current directory of the chooser
                fileChooser.setCurrentDirectory(new File("."));
                // Show the system "Open" dialog
                fileChooser.showSaveDialog(null);
                try {
                    // Get the selected file
                    File file = fileChooser.getSelectedFile();
                    file.canRead();
                    _adaptador.loadFromFile(file);
                    printSuccess("Info loaded successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        } else {
    
            while (true) {
                try {
                    File file = new File(input(sc, "Open path (file):\n"));
                    file.canRead();
                    _adaptador.loadFromFile(file);
                    printSuccess("Info loaded successfully.\n");
                    break;
                } catch (Exception e) {
                    printError("Error: Can't use the file.\n");
                    e.printStackTrace();
                    if (!input(sc, "Try again? Y/n\n").equalsIgnoreCase("y")) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Gestiona el menú principal de l'aplicació.
     */

    public void gestioMercatUB() {
        Scanner sc = new Scanner(System.in);
        Menu<llistaOpcions> menu = new Menu<>("MercatUB", llistaOpcions.values());
        menu.setDescripcions(descripcions);

        llistaOpcions opcio = null;
        while (opcio != llistaOpcions.MENU_SORTIR) {

            menu.mostrarMenu();
            opcio = (llistaOpcions) menu.getOpcio(sc);

            switch (opcio) {
                case MENU_GESTIO_ARTICLES:
                    gestio_articles(sc);
                    break;
                case MENU_GESTIO_CLIENTS:
                    gestio_clients(sc);
                    break;
                case MENU_GESTIO_COMANDES:
                    gestio_comandes(sc);
                    break;
                case MENU_GUARDAR_DADES:
                    guardar_dades(sc);
                    break;
                case MENU_CARREGA_DADES:
                    carrega_dades(sc);
                    break;
                default:
                    break;
            }

        }

    };
}
