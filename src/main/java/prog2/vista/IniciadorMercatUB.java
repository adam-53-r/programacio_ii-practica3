package prog2.vista;

public class IniciadorMercatUB {
    public static void main(String[] args) {
        
        try {
            if (args[0].equals("--nogui")) {
                MercatUB mercat = new MercatUB();
                mercat.gestioMercatUB();
                return;
            }
        } catch (Exception e) {}
        
        AppMercatUB.main(null);
        
    }
}
