package prog2.model;

import java.io.Serializable;
import java.util.ArrayList;
import prog2.vista.MercatException;

public class Llista<T> implements Serializable {
   protected ArrayList<T> _llista;

   public Llista() {
      _llista = new ArrayList<>();
    }

    /**
     * Retorna tamany de la llista.
     */
    public int getSize() {
          return _llista.size();
    }

    /**
     * Afegeix a la llista
     * @param t
     */
    public void afegir(T t) throws MercatException {
          _llista.add(t);
    }

    /**
     * Retorna posició de la llista
     * @param position
     */
    public T getAt(int position) {
          return _llista.get(position);
    }

    /**
     * Buida la llista
     */
    public void clear() {
        _llista.clear();
    }

    /**
     * Indica si la llista està buida.
     */
    public boolean isEmpty() {
          return _llista.isEmpty();
    }

    /**
     * Crea un objecte de ArrayList i el retorna
     */
    public ArrayList<T> getArrayList() {
        ArrayList<T> arrlist = new ArrayList<>(_llista);
        return arrlist;
    }
}
