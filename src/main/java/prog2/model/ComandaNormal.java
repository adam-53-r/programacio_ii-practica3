package prog2.model;

public class ComandaNormal extends Comanda {

    private int tempsRecorregut;

    /**
     * @param _customer
     * @param _product
     * @param _quantity
     * Es genera un constructor que hereta de Comanda i per defecte el tempsRecorregut
     * inicialitzat a 0 per tenir un control de la comanda.
     */
    public ComandaNormal(Client _customer, Article _product, int _quantity) {
        super(_customer, _product, _quantity);
        this.tempsRecorregut = 0; // EN SEGUNDOS (int)getCreation_time()/1000;
    }
    /**
     * Retorna un booleà que indica si la comanda ha sigut enviada o no segons el temps recorregut i enviament.
     */

    @Override
    public boolean comandaEnviada() {
        return tempsRecorregut < tempsEnviament();
    }

    /**
     * Métode que retorna un boleà si la comanda ha sigut rebuda segons el temps recorregut i rebuda.
     */
    @Override
    public boolean comandaRebuda() {
        return tempsRecorregut == tempsEnviament()+tempsRebuda();
    }

    /**
     * Métode que retorna el preu del enviament segons la comanda normal (1)
     */

    @Override
    public float preuEnviament() {
        return 1.0f;
    }

    /**
     * Métode que retorna el temps que triga el enviament.
     */

    @Override
    public int tempsEnviament() {
        int tempsEnvio = (int)this.getProduct().getSend_time().toSeconds();
        tempsRecorregut += tempsEnvio;
        return tempsEnvio;
    }
    /**
     * Métode que retorna el temps que triga en que la comanda sigui rebuda.
     */

    @Override
    public int tempsRebuda() {
        tempsRecorregut+= 120;
        return 120; // En segons
    }

    /**
     * Métode que retorna que la comanda es de tipus normal.
     */

    @Override
    public String tipusComanda() {
        return "Normal";
    }
}
