package prog2.model;

import java.util.Date;
import java.util.TimerTask;

/**
 *
 * @author dortiz
 */
public class RebComandaTimerTask extends TimerTask {
    private Comanda comanda;

    /**
     * @param comanda
     */
    public RebComandaTimerTask(Comanda comanda){
        this.comanda = comanda;
    }

    /**
     * Comprova si la comanda ha sigut rebuda.
     */
    @Override
    public void run() {
        this.comanda.setReceived(true);
        this.comanda.set_receive_time(new Date());
        System.out.println("Comanda Rebuda: " + this.comanda);
    }

}
