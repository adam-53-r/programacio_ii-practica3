package prog2.model;

import java.io.Serializable;
import java.util.ArrayList;

import prog2.vista.MercatException;

public class Dades implements InDades, Serializable {

    private LlistaComandes _llistaComandes;
    private LlistaArticles _llistaArticles;
    private LlistaClients _llistaClients;

    /**
     * Es genera un constructor on s'inicialitzen les llistes de comandes,
     * articles i clients.
     */
    public Dades() {
        _llistaComandes = new LlistaComandes();
        _llistaArticles = new LlistaArticles();
        _llistaClients = new LlistaClients();
    }

    /**
     * @param id
     * @param nom
     * @param preu
     * @param temps
     * @param admetUrgent
     * Métode on crea l'objecte Article i l'afegeix a la llista d'Articles.
     */

    @Override
    public void afegirArticle(String id, String nom, float preu, int temps, boolean admetUrgent) throws MercatException {
        _llistaArticles.afegir(new Article(nom, preu, admetUrgent, temps));
    }

    /**
     * Métode que retorna la llista de articles.
     */
    @Override
    public ArrayList<Article> recuperaArticles() {
        return _llistaArticles.getArrayList();
    }

    /**
     * @param email
     * @param nom
     * @param adreca
     * @param esPremium
     * Métode que crea un objecte de Client (segons si es Premium o Estandard) i l'afegeix a la llista
     */

    @Override
    public void afegirClient(String email, String nom, String adreca, boolean esPremium) throws MercatException {
        if (esPremium) {
            _llistaClients.afegir(new ClientPremium(email, nom, adreca));
        }
        else {
            _llistaClients.afegir(new ClientEstandard(email, nom, adreca));
        }        
    }

    /**
     * Métode que retorna la llista de clients
     */
    @Override
    public ArrayList<Client> recuperaClients() {
        return _llistaClients.getArrayList();
    }
    
    /**
     * @param articlePos
     * @param clientPos
     * @param quantitat
     * @param esUrgent
     * Crea l'objecte comanda segons si es urgent o no i l'afegeix a la llista de comandes.
     */

    @Override
    public void afegirComanda(int articlePos, int clientPos, int quantitat, boolean esUrgent) throws MercatException {
        if (esUrgent) {
            _llistaComandes.afegir(new ComandaUrgent(_llistaClients.getAt(clientPos), _llistaArticles.getAt(articlePos), quantitat));
        }
        else {
            _llistaComandes.afegir(new ComandaNormal(_llistaClients.getAt(clientPos), _llistaArticles.getAt(articlePos), quantitat));
        }
    }

    /**
     * @param position
     * Rep la posició de la comanda i la cancel.la.
     */
    @Override
    public void cancelarComanda(int position) throws MercatException {
        _llistaComandes.getAt(position).cancela();
    }

    /**
     * Retorna i recupera la llista de comandes.
     */

    @Override
    public ArrayList<Comanda> recuperaComandes() {
        return _llistaComandes.getArrayList();
    }

    /**
     * Retorna i recupera la llista de comandes cancelades
     */

    @Override
    public ArrayList<Comanda> recuperaComandesCancelades() {

        ArrayList<Comanda> output = new ArrayList<>();

        _llistaComandes.getArrayList().forEach( (comanda) -> {
            if (comanda.isCanceled()) {
                output.add(comanda);
            }
        });

        return output;
    }
}