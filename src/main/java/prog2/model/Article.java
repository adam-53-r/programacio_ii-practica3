package prog2.model;

import java.io.Serializable;
import java.time.Duration;
import java.util.UUID;

public class Article implements Serializable {
    
    private String _id;
    private String _name;
    private float _price;
    private boolean _allow_urgent;
    private Duration _send_time; // Si da problemas se utiliza private int _sendTime;
    
    
    /**
     * @param _name
     * @param _price
     * @param _allow_urgent
     * @param _send_time
     */
    public Article(String _name, float _price, boolean _allow_urgent, int _send_time) {
        this._id = UUID.randomUUID().toString();
        this._name = _name;
        this._price = _price;
        this._allow_urgent = _allow_urgent;
        this._send_time = Duration.ofSeconds(_send_time);
    }


    /**
     * @return the id
     */
    public String getId() {
        return _id;
    }


    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this._id = id;
    }


    /**
     * @return the name
     */
    public String getName() {
        return _name;
    }


    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this._name = name;
    }


    /**
     * @return the price
     */
    public float getPrice() {
        return _price;
    }


    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this._price = price;
    }


    /**
     * @return the allow_urgent
     */
    public boolean isAllow_urgent() {
        return _allow_urgent;
    }


    /**
     * @param allow_urgent the allow_urgent to set
     */
    public void setAllow_urgent(boolean allow_urgent) {
        this._allow_urgent = allow_urgent;
    }


    /**
     * @return the send_time
     */
    public Duration getSend_time() {
        return _send_time;
    }


    /**
     * @param send_time the send_time to set
     */
    public void setSend_time(Duration send_time) {
        this._send_time = send_time;
    }

    /**
     * Métode que retorna en forma de cadena de caràcters totes les dades sobre el article.
     */
    public String toString() {
        return String.format(
            "ID: %s, Nom=%s, Preu = %.2f€, Temps fins enviament = %s segons, Enviament Urgent = %s",
            getId(), getName(), getPrice(), getSend_time().getSeconds(), isAllow_urgent() ? "Si" : "No"
        );
    }
    

}
