package prog2.model;

import java.util.Iterator;

import prog2.vista.MercatException;

public class LlistaArticles extends Llista<Article> {
    
    public LlistaArticles() {
        super();
    }

    /**
     * Afegeix el article a la llista d'articles.
     * No es podran afegir 2 articles amb el mateix identificador.
     * @param article
     */
    @Override
    public void afegir(Article article) throws MercatException {
        
        for (Iterator<Article> it = _llista.iterator(); it.hasNext();) {
            if ( article.getId().equals( it.next().getId() ) ) {
                throw new MercatException("Article with same id exists.");
            }
        }
        super.afegir(article);
    }

}