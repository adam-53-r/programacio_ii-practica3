package prog2.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Timer;

/**
 *
 * @author dortiz
 */
public abstract class Comanda implements Serializable {

    private Client _customer;
    private Article _product;
    private int _quantity;
    private Date _creation_time;
    private Date _send_time;
    private Date _receive_time;
    private boolean _sent;
    private boolean _received;
    private boolean _canceled;

    public Comanda(Client _customer, Article _product, int _quantity){
        this._customer = _customer;
        this._product = _product;
        this._quantity = _quantity;
        this._creation_time = new Date();
        this._send_time = null;
        this._receive_time = null;
        this._sent = false;
        this._received = false;
        this._canceled = false;
        this.processa();
    }

    /**
     * @return the customer
     */
    public Client getCustomer() {
        return _customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(Client customer) {
        this._customer = customer;
    }

    /**
     * @return the product
     */
    public Article getProduct() {
        return _product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Article product) {
        this._product = product;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return _quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this._quantity = quantity;
    }

    /**
     * @return the creation_time
     */
    public Date getCreation_time() {
        return _creation_time;
    }

    /**
     * @param creation_time the creation_time to set
     */
    public void setCreation_time(Date creation_time) {
        this._creation_time = creation_time;
    }

    /**
     * @return the _send_time
     */
    public Date get_send_time() {
        return _send_time;
    }

    /**
     * @param _send_time the _send_time to set
     */
    public void set_send_time(Date _send_time) {
        this._send_time = _send_time;
    }

    /**
     * @return the _receive_time
     */
    public Date get_receive_time() {
        return _receive_time;
    }

    /**
     * @param _receive_time the _receive_time to set
     */
    public void set_receive_time(Date _receive_time) {
        this._receive_time = _receive_time;
    }

    /**
     * @return the sent
     */
    public boolean isSent() {
        return _sent;
    }

    /**
     * @param sent the sent to set
     */
    public void setSent(boolean sent) {
        this._sent = sent;
    }

    /**
     * @return the received
     */
    public boolean isReceived() {
        return _received;
    }

    /**
     * @param received the received to set
     */
    public void setReceived(boolean received) {
        this._received = received;
    }

    /**
     * @return the canceled
     */
    public boolean isCanceled() {
        return _canceled;
    }

    /**
     * @param canceled the canceled to set
     */
    public void setCanceled(boolean canceled) {
        this._canceled = canceled;
    }

    public void processa(){
        boolean daemon = true;
        Timer timer = new Timer(daemon); // Create timer with a daemon thread to avoid blocking program termination
        long tempsEnvMilisegs = tempsEnviament() * 1000;
        timer.schedule(new EnvComandaTimerTask(this), tempsEnvMilisegs);
    }

    public void cancela() {
        _canceled = true;
    }

    public abstract int tempsEnviament();
    
    public abstract int tempsRebuda();
    
    public float calcPreu() {
        return _product.getPrice() * _quantity;
    };

    public abstract String tipusComanda();
    
    public abstract boolean comandaEnviada();
    
    public abstract boolean comandaRebuda();
    
    public abstract float preuEnviament();

    /**
     * Métode que retorna en forma de cadena de caràcters totes les dades sobre la comanda.
     */
    public String toString() {
        return String.format("Tipus: %s, Article: %s, Client: %s, Quantitat: %d, Data de creació: %s, Data d'enviament: %s, Data de recepcio: %s, Enviada: %s, Rebuda: %s, Cancel.lada: %s, Preu Articles: %.2f€, Preu Enviament: %.2f€",
                tipusComanda(),getProduct(),getCustomer(),getQuantity(),getCreation_time(),get_send_time(),get_receive_time(),isSent(),isReceived(),isCanceled(),calcPreu(),preuEnviament());
    }
}
