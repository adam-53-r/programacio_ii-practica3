package prog2.model;

public class ClientEstandard extends Client {

    public ClientEstandard(String _email, String _name, String _address) {
        super(_email, _name, _address);
    }

    /**
     * No es paguen mensualitats
     */
    @Override
    public float calcMensual() {
        return 0.0f;
    }

    /**
     * No té cap tipus de descompte
     */
    @Override
    public float descompteEnv() {
        return 0.0f;
    }

    /**
     * Indica que el client es Estandard.
     */
    @Override
    public String tipusClient() {
        return "Estandard";
    }
    
}
