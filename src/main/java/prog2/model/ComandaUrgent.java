package prog2.model;

public class ComandaUrgent extends Comanda {

    private int _tempsRecorregut;
    /**
     * @param _customer
     * @param _product
     * @param _quantity
     * Es genera un constructor que hereta de Comanda i per defecte el tempsRecorregut
     * inicialitzat a 0 per tenir un control de la comanda.
     */

    public ComandaUrgent(Client _customer, Article _product, int _quantity) {
        super(_customer, _product, _quantity);
        this._tempsRecorregut = 0;
    }

    /**
     * Retorna un booleà que indica si la comanda ha sigut enviada o no segons el temps recorregut i enviament.
     */
    @Override
    public boolean comandaEnviada() {
        return _tempsRecorregut < tempsEnviament();
    }
    /**
     * Métode que retorna un boleà si la comanda ha sigut rebuda segons el temps recorregut i rebuda.
     */
    @Override
    public boolean comandaRebuda() {
        return _tempsRecorregut == tempsEnviament()+tempsRebuda();
    }
    /**
     * Métode que retorna el preu del enviament segons la comanda urgent (4)
     */
    @Override
    public float preuEnviament() {
        return 4.0f;
    }
    /**
     * Métode que retorna el temps que triga el enviament.
     */
    @Override
    public int tempsEnviament() {
        int tempsEnvio = (int)this.getProduct().getSend_time().toSeconds();
        _tempsRecorregut += tempsEnvio;
        return tempsEnvio;
    }
    /**
     * Métode que retorna el temps que triga en que la comanda sigui rebuda.
     */
    @Override
    public int tempsRebuda() {
        _tempsRecorregut+= 60;
        return 60; // En segons
    }
    /**
     * Métode que retorna que la comanda es de tipus urgent.
     */

    @Override
    public String tipusComanda() {
        return "Urgent";
    }
    
}
