package prog2.model;

import java.util.Iterator;

import prog2.vista.MercatException;

public class LlistaClients extends Llista<Client> {
    public LlistaClients() {
        super();
    }

    /**
     * Afegeix el client a la llista de clients
     * No es podran afegir 2 clients amb el mateix correu electrónic
     * @param client
     */
    @Override
    public void afegir(Client client) throws MercatException{
        for (Iterator<Client> it = _llista.iterator(); it.hasNext();) {
            if (client.getEmail().equals( it.next().getEmail() ) ) {
                throw new MercatException("Client with same email exists.");
            }
        }
        super.afegir(client);
    }

}