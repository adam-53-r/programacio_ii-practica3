package prog2.model;

public class ClientPremium extends Client {

    public ClientPremium(String _email, String _name, String _address) {
        super(_email, _name, _address);
    }

    /**
     * Es paguen 4€ al mes
     */
    @Override
    public float calcMensual() {
        return 4.0f;
    }

    /**
     * Reben un descompte del 20% en les despeses d'enviament
     */
    @Override
    public float descompteEnv() {
        return 20.0f;
    }

    /**
     * Indica que el client es tipus premium
     */

    @Override
    public String tipusClient() {
        return "Premium";
    }
    
}
