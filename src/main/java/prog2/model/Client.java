package prog2.model;

import java.io.Serializable;

public abstract class Client implements Serializable {
    
    private String _email;
    private String _name;
    private String _address;
    
    /**
     * @param _email
     * @param _name
     * @param _address
     */
    public Client(String _email, String _name, String _address) {
        this._email = _email;
        this._name = _name;
        this._address = _address;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return _email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this._email = email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return _name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this._name = name;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return _address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this._address = address;
    }
    
    public abstract String tipusClient();
    public abstract float calcMensual();
    public abstract float descompteEnv();

    /**
     * Métode que retorna en forma de cadena de caràcters totes les dades sobre el client.
     */

    public String toString(){
        return String.format("Tipus: %s, Email: %s, Nom: %s, Adreça: %s, Descompte Enviament: %.2f€, Mensualitat: %.2f€",
                tipusClient(),getEmail(),getName(),getAddress(),descompteEnv(),calcMensual());
    }
}
