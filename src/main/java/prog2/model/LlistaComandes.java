package prog2.model;

import java.io.Serializable;

import prog2.vista.MercatException;

public class LlistaComandes extends Llista<Comanda> {
    
    public LlistaComandes() {
        super();
    }

    /**
     * Afegeix i comprova si la comanda es urgent
     * No es podrá afegir un objecte de tipus ComandaUrgent si l'article que ha d'enviar-se no admet enviament urgent.
     * @param comanda
     */
    @Override
    public void afegir(Comanda comanda) throws MercatException {
        
        if (comanda instanceof ComandaUrgent && !comanda.getProduct().isAllow_urgent()) {
            throw new MercatException("The article doesn't allow urgent delivery.");
        }
        super.afegir(comanda);
    }
}